# Ticks_and_R_package
This repository is for containing tick data and R codes


``` r setup, include=FALSE
knitr::opts_chunk$set(echo = TRUE)

```

# Le package tempoFrameshift

  Ce document décrit l'utilité et le fonctionnement du package tempoFrameshift, élaboré en cours de stage pour analyser des données d'abondance de tiques.


``` r
library(tempoFrameshift)
library(ggplot2)
```


# 1.Simulation des données

  Premièrement, une fonction de simulation de données a été créée afin de tester les fonctions du package. La fonction `lagged_XPo_simul()` simule une variable réponse Y qui dépend d'une variable explicative x1. La fonction retourne une liste contenant un tableau avec les variables d'exposition (`sim$x`) et un tableau avec toutes les variables datées (`sim$y`)

``` r
sim <- lagged_XPo_simul(end_time = Sys.Date(),
                             n_times = 800,
                             x_start = 8 ,
                             x_end = 5,
                             FUN = mean,
                             coF = 2.4)

```

  Les résultats de la simulation sont : 
  
``` r
head(sim$x)
```
    ## # A tibble: 809 × 4
    ##   exposure_date   day day_rad    x1
    ##   <date>        <int>   <dbl> <dbl>
    ## 1 2022-02-12       43   0.740 0.776
    ## 2 2022-02-13       44   0.757 0.776
    ## 3 2022-02-14       45   0.775 0.633
    ## 4 2022-02-15       46   0.792 0.666
    ## 5 2022-02-16       47   0.809 0.760
    ## 6 2022-02-17       48   0.826 0.663
    ## 7 2022-02-18       49   0.843 0.667
    ## 8 2022-02-19       50   0.861 0.675
    ## 9 2022-02-20       51   0.878 0.629
    ## 10 2022-02-21       52   0.895 0.474
    ## # ℹ 799 more rows
    ## # ℹ Use `print(n = ...)` to see more rows
    
``` r
head(sim$y)
```
    ## # A tibble: 802 × 3
    ##   y_date     x1_aggr     y
    ##   <date>       <dbl> <dbl>
    ## 1 2022-02-20   0.775  1.86
    ## 2 2022-02-21   0.732  1.76
    ## 3 2022-02-22   0.759  1.82
    ## 4 2022-02-23   0.734  1.76
    ## 5 2022-02-24   0.720  1.73
    ## 6 2022-02-25   0.708  1.70
    ## 7 2022-02-26   0.638  1.53
    ## 8 2022-02-27   0.587  1.41
    ## 9 2022-02-28   0.580  1.39
    ## 10 2022-03-01   0.609  1.46
    ## # ℹ 792 more rows
    ## # ℹ Use `print(n = ...)` to see more rows
    

# 2. Vers la heatmap

## 2.1. Conceptualisation

  Afin de créer la heatmap d'intérêt, il est nécessaire de définir un début et une fin d'intervalle. Ces derniers correspondent à un intervalle de temps qu'on remonte à partir d'une valeur de $Y_i$. Par exemple, si on a : 
  
    - start.int = 10
    - end.int = 5
    
cela veut dire que l'on prend un intervalle entre - 10 et - 5 jours à partir de $Y_i$. On a donc un lag 10.

  Le but est ensuite d'aggréger les données de x1 à l'intérieur de l'intervalle par une fonction comme `FUN = mean` (moyenne), ce qui nous donne une valeur aggrégée de x1 pour une valeur Yi et on effectue cette procédure pour chaque valeur de Y du jeu de donnée. 
  
  Au final, on obtient un tableau de $Y_i$ avec des x1 aggrégés et on peut en extraire un modèle (e.g. linéaire) ainsi qu'un AIC. En répétant toute la procédure (de la création de l'intervalle jusqu'à l'AIC) pour chaque combinaison de start.int et end.int, on peut obtenir une heatmap des AIC. 
  
Aggréger : appliquer une fonction à une série de données, les résumant en une valeur (e.g. la moyenne, la médiane...)
  

## 2.2. Démonstration

  Le tableau *Y* sera utilisé pour l'exemple. La fonction `lagged_lm` permet de créer un tableau contenant 3 choses : chaque début d'intervalle, chaque fin d'intervalle et l'AIC correspondant. Via la fonction `expand.grid`, la fonction génère toutes les combinaisons possibles d'intervalles à partir des débuts et des fins (mais à la condition que $start.int \ge end.int$ ; cette condition est à l'origine de la forme triangulaire de la heatmap)
  
  On commence donc par générer le tableau des AIC via la fonction `lagged_lm`, ce qui donne le tableau suivant : 

``` r
exlm <- lagged_lm(formula = y ~ x1_aggr,
                    outcome_data = out_data,
                    outcome_time_col = "y_date",
                    exposure_data = expo_data,
                    exposure_time_col = "exposure_date",
                    exposure_var = "x1",
                    time_unit = "days",
                    max_int = 8L, min_int = 4L,
                    FUN = mean)
head(exlm)
```
    ## # A tibble: 15 × 3
    ##    start_int end_int    AIC
    ##        <int>   <int>  <dbl>
    ##  1         4       4 81014.
    ##  2         5       5 80571.
    ##  3         5       4 80091.
    ##  4         6       6 80516.
    ##  5         6       5 79536.
    ##  6         6       4 79462.
    ##  7         7       7 80466.
    ##  8         7       6 79410.
    ##  9         7       5 78743.
    ## 10         7       4 78860.
    ## 11         8       8 80465.
    ## 12         8       7 79346.
    ## 13         8       6 78577.
    ## 14         8       5 77877.
    ## 15         8       4 78213.


  Ensuite, la fonction `tempoFrameshift` utilise `lagged_lm` pour générer une heatmap où l'on peut visualiser les meilleurs modèles. Les AIC les plus faibles sont représentés en rouge tandis que les plus hauts sont en jaune. 

``` r
heatmp <- tempoFrameshift::tempoFrameshift(formula = y ~ x1_aggr, 
                                             outcome_data = out_data, 
                                             outcome_time_col = "y_date",
                                             exposure_data = expo_data, 
                                             exposure_time_col = "exposure_date",
                                             exposure_var = "x1", 
                                             time_unit = "days", 
                                             max_int = 12L, min_int = 1L, 
                                             FUN = mean)
heatmp
```

![heatmapV2_good](https://github.com/rusach/Ticks_and_R_package/assets/167363032/cef5e477-5024-41d8-89c7-5332ff4a1bb1)

Enfin, la fonction `best_model_researcher` prend les arguments communs à `tempoFrameshift` et `lagged_lm` ainsi que le tableau des AIC pour retourner le meilleur modèle avec son AIC. 

``` r
bm  <- best_model_researcher(AIC_df = exlm,
                formula = y ~ x1_aggr,
                outcome_data = out_data,
                outcome_time_col = "y_date",
                exposure_data = expo_data,
                exposure_time_col = "exposure_date",
                exposure_var = "x1",
                time_unit = "days",
                max_int = 8L, min_int = 4L,
                FUN = mean)

bm
```
    ## [[1]]
    ## 
    ## Call:
    ## lm(formula = formula, data = data_model)
    ## 
    ## Coefficients:
    ## (Intercept)      x1_aggr  
    ##   3.844e-16    2.400e+00  
    ## 
    ## 
    ## [[2]]
    ## [1] 1763.171


