
# lagged_model -----------------------------------------------------------

#' AIC of lagged intervals
#'
#' @description
#' This function aims to help understanding what prior time interval controls an outcome variable. In
#' order to do that, the functon creates a subset of data based on the datasets taken into parameters,
#' and taking the lag/delay of time into account ; it generates a model and calculates an AIC score
#' Each possible interval has an AIC and finding which is the better model is possible by using
#' the function `best_model_researcher` from the `time.windowing` package (the same package containing
#' `lagged_model`) or simply by plotting an heatmap with the command `ggplot(data)`.
#'
#'
#' @param modtype the type of model the function has to perfom,like `lm`, `glm`...
#' @param outcome_data the outcome dataset
#' @param exposure_data the exposure dataset
#' @param outcome_time_col a character, the name of the column containing the time data of the
#' outcome dataset
#' @param exposure_time_col a character, the name of the column containing the time data of the
#' exposure dataset
#' @param outcome_hour_col a character, the name of the column containing the hours of the
#' outcome dataset
#' @param exposure_hour_col a character, the name of the column containing the hours of the
#' exposure dataset
#' @param outcome_var the name of the outcome variable
#' @param exposure_var the name of the exposure variable
#' @param formula a formula for the model that is injected in modtype. The syntax is : y ~ x
#' @param time_unit a character, the unit of time such as "hours", "days", "weeks"... By default,
#' the parameter is set on "days"
#' @param max_int an integer, the superior limit of the interval
#' @param min_int an integer, the inferior limit of the interval
#' @param FUN a function of aggregatio such as sum, mean, median...
#'
#' @return an object of class tempFS. It contains a) the tibble with the AIC of each interval
#'b) the time_unit, c) the outcome variable and d) the exposure variable
#' @export
#'
#' @examples
#'
#' sim <- lagged_XPo_simul(end_time = Sys.Date(),n_times = 1000, max_int = 7, min_int = 5, FUN = mean, coF = 1, time_unit = "days")
#'
#' expo <- sim$x
#' outco <- sim$y
#'
#' lagged_model(modtype = lm, outcome_data = outco, outcome_time_col = "y_date", outcome_var = "y",
#' exposure_data = expo, exposure_time_col = "exposure_date", exposure_var = "x1", exposure_hour_col = "hour_UTC",
#' outcome_hour_col = "hour_UTC", formula = y ~ 1, time_unit = "days", max_int = 10L, min_int = 5L,
#' FUN = mean)
#'

lagged_model <- function(modtype = lm,
                         outcome_data,
                         exposure_data,
                         outcome_time_col,
                         exposure_time_col,
                         outcome_hour_col,
                         exposure_hour_col,
                         outcome_var,
                         exposure_var,
                         formula = y ~ poly(x1_aggr, 1),
                         time_unit = "days",
                         max_int = 20L,
                         min_int = 10L,
                         FUN = mean){


  ### Stop the function if max_int < min_int
  if (max_int < min_int){
    stop("Error : your max_int value has to be greater than the min_int value !
         The max_int is the superior limit of the intervals while min_in is the inferior limit")

  }

  if ((max_int < 0) | (min_int < 0)){
    stop("Error : max_int and min_int must be positive")
  }

  if ((time_convertor(max_int, time_unit) > 372) &
      (time_unit %in% c("hours", "days", "weeks","months"))){

    stop("Error : the shiffting must be inferior to one year")
  }


  ## Standardisation of the column names is more handy  :
  index_ov <- which(colnames(outcome_data) == outcome_var)
  index_ev <- which(colnames(exposure_data) == exposure_var)
  colnames(outcome_data)[index_ov] <- "y"
  colnames(exposure_data)[index_ev] <- "x1"

  ## Make the date and hour columns readible by R
  exposure_data <- special_asDate(df = exposure_data,
                                  df_time_col = exposure_time_col,
                                  hour_col = exposure_hour_col,
                                  time_unit = time_unit)

  outcome_data <- special_asDate(df = outcome_data,
                                 df_time_col = outcome_time_col,
                                 hour_col = outcome_hour_col,
                                 time_unit = time_unit)

  ## Select the interesting columns

  expo <- exposure_data |>
    dplyr::select(as.symbol(exposure_time_col), x1)

  outco <- outcome_data |>
    dplyr::select(as.symbol(outcome_time_col), y)

  ## Determine the limit dates
  time_first_o <- min(outcome_data[[outcome_time_col]], na.rm = TRUE)
  time_first_e <- min(exposure_data[[exposure_time_col]], na.rm = TRUE)
  time_first   <- min(c(time_first_o, time_first_e))
  time_last    <- max(outcome_data[[outcome_time_col]], na.rm = TRUE)

  ## Case when outcome values don't have prior exposure values ; the outcome data should be
  ## truncated to have prior data in the exposure dataset
  ## OR Case when difftime of the two dates is < to max_int

  ## Number of days separing the two first days
  difft <- - as.integer(difftime(time1 = time_first_e, time2 = time_first_o, units = "days"))

  ## Required number of days separing the two first days
  difft_req <- ifelse(time_unit != "hours",
                       time_convertor(max_int, time_unit),
                       max_int) ## difftime recquired by max_int

  if (time_first_o < time_first_e){
    message("Warning : While the outcome data starts sooner than the exposure data, the outcome data was
            shortened in orter to do predictions !")

    outco <-  outco |>
      dplyr::filter(outco[[outcome_time_col]] >= time_first_e + max_int)


    time_first <- time_first_e

  } else if (difft < difft_req) {
    message("Warning : The difference of days between the first dates of the two dataset is
            inferior to max_int. The outcome dataset is thus shortened")

    outco <-  outco |>
      dplyr::filter(outco[[outcome_time_col]] >= time_first_e + difft_req)

    time_first <- time_first_e

  }

  ## A tibble to join for the date ID from outcome_data
  if (time_unit == "hours"){
    ## The dates are extracted from the merge of all the dates
    vec_datetime <- unique(c(expo[[exposure_time_col]], outco[[outcome_time_col]]))

    time_seq <- tibble::tibble(
      Date = as.POSIXct(vec_datetime),
      date_ID = 1:length(Date)
    )

  } else {
    ## The dates are extracted from the merge of all the dates
    vec_date <- unique(c(expo[[exposure_time_col]], outco[[outcome_time_col]]))

    time_seq <- tibble::tibble(
      Date = as.POSIXct(vec_date),
      date_ID = 1:length(Date)
    )
  }

  ## Rename time columns to join the df
  expo <- expo |>
    dplyr::rename(Date = exposure_time_col)
  outco <- outco |>
    dplyr::rename(Date = outcome_time_col)

  ## Joint of time_seq to expo df and outco df
  expo <- dplyr::left_join(expo, time_seq) |>
    dplyr::select(date_ID, x1) |>
    dplyr::rename(lag_id = date_ID)

  outco <- dplyr::left_join(outco, time_seq) |>
    dplyr::select(date_ID, y)


  ## Aggregation of exposure values for each date (if hours do not matter)
  if (time_unit != "hours"){

    expo <- expo |>
      dplyr::group_by(lag_id) |>
      dplyr::summarise(
        x1 = FUN(x1, na.rm = TRUE)
      )
  }

  ## The null model and its AIC
  formula0 <- y ~ 1
  null_model <- lm(formula0, data = outco)
  null_AIC <- AIC(null_model)
  formula0 <- formula

  ## the final dataframe that will contain the AIC scores. For now, it
  ## contains all combination of start and end
  all_intervals <- tidyr::expand_grid(
    start_int = max_int:min_int,
    end_int = max_int:min_int,
    AIC = rep(NA)
  )

  ## Reduce the df for a triangular heatmap
  all_intervals <- all_intervals[all_intervals$end_int <= all_intervals$start_int,] |>
    dplyr::arrange(start_int)

  for (i in (1:nrow(all_intervals))){

    ## Retrieve start_int and end_int
    start_int = all_intervals$start_int[i]
    end_int = all_intervals$end_int[i]

    ## Convert the weeks in numbers of days
    start_int <- ifelse(time_unit != "hours",
                        time_convertor(start_int, time_unit),
                        start_int)

    end_int <- ifelse(time_unit != "hours",
                      time_convertor(end_int, time_unit),
                      end_int)

    ## Enables to decompose all the days in each interval
    joined_df <- tidyr::expand_grid(outco, time_lag = start_int:end_int)

    ## Retrieve the id after the lag
    joined_df <- joined_df |>
      dplyr::mutate(lag_id = date_ID - time_lag) |>
      dplyr::select(date_ID,lag_id)

    joined_df <- dplyr::left_join(joined_df,expo)

    ## Calculates the aggregated values for each intervals
    aggrega_df <- joined_df |>
      dplyr::group_by(date_ID) |>
      dplyr::summarise(
        x1_aggr = FUN(x1, na.rm = TRUE)
      ) |>
      dplyr::ungroup()

    outco <- tibble::as_tibble(outco)

    ## Creation of a subset of data for the model
    data_model <- dplyr::left_join(outco, aggrega_df, by = "date_ID")

    ## Creation of the model and calculation of the AIC reduced from the null_AIC
    model <- modtype(formula0, data = data_model)

    ## The score is ranged in the tibble all_intervals
    all_intervals$AIC[i] <- AIC(model) - null_AIC

  }
  ## The results are packed in a tempFS object
  result <- tempFS_df(array = all_intervals,
                        unit = time_unit,
                        outc = outcome_var,
                        expos = exposure_var)
  return(result)
}


# best_model_researcher ---------------------------------------------------


#' Best model from a tempFS object
#'
#' @description
#' This function finds the best model from a tempFS object containing a dataframe of the AIC of
#' lagged intervals.
#'
#'
#' @param tempFS_obj an object of class tempFS containing the AIC dataframe
#' @inheritParams lagged_model
#'
#' @return a list of 4 objects : a) the best model and b) its AIC score, c) the start of interval
#' and d) the end of interval
#' @export
#'
#' @examples
#' sim <- lagged_XPo_simul(end_time = Sys.Date(),n_times = 1000, max_int = 7, min_int = 5, FUN = mean, coF = 1, time_unit = "days")
#'
#' expo <- sim$x
#' outco <- sim$y
#'
#' lagM <- lagged_model(modtype = lm, outcome_data = outco, outcome_time_col = "y_date", outcome_var = "y",
#' exposure_data = expo, exposure_time_col = "exposure_date", exposure_var = "x1", exposure_hour_col = "hour_UTC",
#' outcome_hour_col = "hour_UTC", formula = y ~ 1, time_unit = "days", max_int = 10L, min_int = 5L,
#' FUN = mean)
#'
#' best_model_researcher(tempFS_obj = lagM,modtype = lm, outcome_data = outco, outcome_time_col = "y_date", outcome_var = "y",
#' exposure_data = expo, exposure_time_col = "exposure_date", exposure_var = "x1", exposure_hour_col = "hour_UTC",
#' outcome_hour_col = "hour_UTC", formula = y ~ 1, time_unit = "days", max_int = 10L, min_int = 5L,
#' FUN = mean)
#'
best_model_researcher <- function(tempFS_obj,
                                  modtype = lm,
                                  outcome_data,
                                  exposure_data,
                                  outcome_time_col,
                                  exposure_time_col,
                                  outcome_hour_col,
                                  exposure_hour_col,
                                  outcome_var,
                                  exposure_var,
                                  formula = y ~ poly(x1_aggr, 1),
                                  time_unit = "days",
                                  max_int = 20L,
                                  min_int = 10L,
                                  FUN = mean){

  ### Stop the function if max_int < min_int
  if (max_int < min_int){
    stop("Error : your max_int value has to be greater than the min_int value !
         The max_int is the superior limit of the intervals while min_in is the inferior limit")

  }

  if (time_unit == "years"){

    time_unit <- "months"
    max_int <- max_int * 12
    min_int <- min_int * 12
  }

  ## Assignation again
  AIC_df <- tempFS_obj$dataframe


  ## Standardisation of the column names is more handy  :
  index_ov <- which(colnames(outcome_data) == outcome_var)
  index_ev <- which(colnames(exposure_data) == exposure_var)
  colnames(outcome_data)[index_ov] <- "y"
  colnames(exposure_data)[index_ev] <- "x1"

  ## Converts into date type by formatting the columns
  exposure_data <- special_asDate(df = exposure_data,
                                  df_time_col = exposure_time_col,
                                  hour_col = exposure_hour_col,
                                  time_unit = time_unit)

  outcome_data <- special_asDate(df = outcome_data,
                                 df_time_col = outcome_time_col,
                                 hour_col = outcome_hour_col,
                                 time_unit = time_unit)

  ## Select the interesting columns
  expo <- exposure_data |>
    dplyr::select(as.symbol(exposure_time_col), x1)

  outco <- outcome_data |>
    dplyr::select(as.symbol(outcome_time_col), y)

  ## Determine the limit dates
  time_first_o <- min(outcome_data[[outcome_time_col]], na.rm = TRUE)
  time_first_e <- min(exposure_data[[exposure_time_col]], na.rm = TRUE)
  time_first   <- min(c(time_first_o, time_first_e))
  time_last    <- max(outcome_data[[outcome_time_col]], na.rm = TRUE)

  ## Case when outcome values don't have prior exposure values ; the outcome data should be
  ## truncated to have prior data in the exposure dataset
  ## OR Case when difftime of the two dates is < to max_int

  ## Number of days separing the two first days
  difft <- - as.integer(difftime(time1 = time_first_e, time2 = time_first_o, units = "days"))

  ## Required number of days separing the two first days
  difft_req <- ifelse(time_unit != "hours",
                      time_convertor(max_int, time_unit),
                      max_int) ## difftime recquired by max_int

  if (time_first_o < time_first_e){
    message("Warning : While the outcome data starts sooner than the exposure data, the outcome data was
            shortened in orter to do predictions !")

    outco <-  outco |>
      dplyr::filter(outco[[outcome_time_col]] >= time_first_e + max_int)


    time_first <- time_first_e

  } else if (difft < difft_req) {
    message("Warning : The difference of days between the first dates of the two dataset is
            inferior to max_int. The outcome dataset is thus shortened")

    outco <-  outco |>
      dplyr::filter(outco[[outcome_time_col]] >= time_first_e + difft_req)

    time_first <- time_first_e

  }

  ## A tibble to join for the date ID from outcome_data

  if (time_unit == "hours"){
    ## The dates are extracted from the merge of all the dates
    vec_datetime <- unique(c(expo[[exposure_time_col]], outco[[outcome_time_col]]))

    time_seq <- tibble::tibble(
      Date = as.POSIXct(vec_datetime),
      date_ID = 1:length(Date)
    )

  } else {

    time_seq <- tibble::tibble(
      Date = seq.Date(time_first,time_last, by = "days"),
      date_ID = 1:length(Date)
    )
  }

  ## Rename time columns to join the df
  expo <- expo |>
    dplyr::rename(Date = exposure_time_col)
  outco <- outco |>
    dplyr::rename(Date = outcome_time_col)

  ## Joint of time_seq to expo df and outco df
  expo <- dplyr::left_join(expo, time_seq) |>
    dplyr::select(date_ID, x1) |>
    dplyr::rename(lag_id = date_ID)

  outco <- dplyr::left_join(outco, time_seq) |>
    dplyr::select(date_ID, y)

  ## Aggregation of exposure values for each date (if hours do not matter)
  if (time_unit != "hours"){

    expo <- expo |>
      dplyr::group_by(lag_id) |>
      dplyr::summarise(
        x1 = FUN(x1, na.rm = TRUE)
      )
  }

  ## The null model and its AIC
  formula0 <- y ~ 1
  null_model <- lm(formula0, data = outcome_data)
  null_AIC <- AIC(null_model)
  formula0 <- formula

  #####################################

  ## Retrieve the range of the best AIC
  b <- which(min(AIC_df$AIC) == AIC_df$AIC)

  ## Retrieve start_int and end_int
  start_int <- AIC_df$start_int[b]
  end_int <- AIC_df$end_int[b]

  ## Enables to decompose all the days in each interval
  joined_df <- tidyr::expand_grid(outco, time_lag = start_int:end_int)

  ## Retrieve the id after the lag
  joined_df <- joined_df |>
    dplyr::mutate(lag_id = date_ID - time_lag) |>
    dplyr::select(date_ID,lag_id)
  joined_df <- dplyr::left_join(joined_df,expo)


  ## Calculates the aggregated values for each intervals
  aggrega_df <- joined_df |>
    dplyr::group_by(date_ID) |>
    dplyr::summarise(
      x1_aggr = FUN(x1, na.rm = TRUE)
    ) |>
    dplyr::ungroup()

  ## Creation of a subset of data for the model
  data_model <- dplyr::left_join(outco, aggrega_df, by = "date_ID")

  ## Creation of the model and calculation of the AIC reduced from the null_AIC
  best_model <- modtype(formula0, data = data_model)

  best_AIC <- AIC(best_model) - null_AIC

  colnames(data_model)[2:3] <- c(as.character(outcome_var),
                                 as.character(exposure_var))

  result <- tempFS_model(best_model, best_AIC, start_int, end_int, data = data_model)

  return(result)
}


# lagged_analyse ----------------------------------------------------------


#' Analyse of lagged frames
#'
#' @inheritParams lagged_model
#'
#' @return a list with a tempFS_df object and a temp_FS_model object.
#' @export

lagged_analyse <- function(modtype = lm,
                           outcome_data,
                           exposure_data,
                           outcome_time_col,
                           exposure_time_col,
                           outcome_hour_col,
                           exposure_hour_col,
                           outcome_var,
                           exposure_var,
                           formula = y ~ poly(x1_aggr, 1),
                           time_unit = "days",
                           max_int = 20L,
                           min_int = 10L,
                           FUN = mean){

  TFS_AIC <- lagged_model(modtype,
                          outcome_data,
                          exposure_data,
                          outcome_time_col,
                          exposure_time_col,
                          outcome_hour_col,
                          exposure_hour_col,
                          outcome_var,
                          exposure_var,
                          formula,
                          time_unit,
                          max_int,
                          min_int,
                          FUN)

  TFS_model <- best_model_researcher(tempFS_obj = TFS_AIC,
                                     modtype,
                                     outcome_data,
                                     exposure_data,
                                     outcome_time_col,
                                     exposure_time_col,
                                     outcome_hour_col,
                                     exposure_hour_col,
                                     outcome_var,
                                     exposure_var,
                                     formula,
                                     time_unit,
                                     max_int,
                                     min_int,
                                     FUN)

  return(list(TFS_AIC = TFS_AIC,
              TFS_model = TFS_model))

}


# mixed_lagged_model ------------------------------------------------------

mixed_lagged_model <- function(tempFS_objs,
                               formula,
                               outcome_data,
                               outcome_time_col = "y_date",
                               outcome_var,
                               exposure_data,
                               exposure_time_col,
                               exposure_var,
                               exposure_hour_col,
                               outcome_hour_col,
                               time_unit = "days"){


  dim_tfs <- length(tempFS_objs)

  if (dim_tfs == 2){

    ## Extract the tempFS_model objects
    tfs1 <- tempFS_objs[[1]]
    tfs2 <- tempFS_objs[[2]]

    ## Extract data
    data_X1 <- tfs1$data
    data_X2 <- tfs2$data

    ## Rename the column
    colnames(data_X2)[3] <- exposure_var[2]
    ## Square
    data_X2[[exposure_var[2]]] <- data_X2[[exposure_var[2]]]^2

    ## Extract the variables
    X1 <- data_X1[[exposure_var[1]]]
    X2 <- data_X2[[exposure_var[2]]]

    ## Extract the slope
    a <- tfs1$model$coefficients[[2]]
    b <- tfs2$model$coefficients[[2]]

    ## Join all the variables
    all_data <- dplyr::left_join(data_X1,data_X2)

    ## Conversion to log for Y
    assign("y", all_data[[outcome_var]])

    ## Transformation into logarithm
    all_data <- all_data |>
      dplyr::mutate(lnY = log(y) + 1) |>
      dplyr::select(-outcome_var)

    ## Rename the column
    colnames(all_data)[which(colnames(all_data) == "lnY")] <- outcome_var

    ## Replace log(0) into NA
    all_data[[outcome_var]][which(all_data[[outcome_var]] == -Inf)] <- NA

    ## Create the model
    mod <- lm(formula, data = all_data)

    rangeT1 <- round(range(all_data[[exposure_var[1]]], na.rm = TRUE), 0)
    rangeT2 <- round(range(all_data[[exposure_var[2]]], na.rm = TRUE), 0)


    ## The prediction
    pred <- data.frame(x1 = rangeT1[1]:rangeT1[2],
                       x2 = rangeT2[1]:(rangeT2[1] + rangeT1[2]))

    colnames(pred) <- c(exposure_var[1],exposure_var[2])

    pred$y_pred <- predict(mod, pred)

    plot(data = all_data, Ny_Ir ~ temp, col = "black")
    with(pred,
         lines(x = temp,
               y = y_pred,
               col = "red"))
    plot(data = all_data, Ny_Ir ~ temp2, col = "black")
    with(pred,
         lines(x = temp2,
               y = y_pred,
               col = "blue"))

    return(all_data)
  } else if (dim_tfs == 3){

    ## Extract the tempFS_model objects
    tw1 <- tempFS_objs[[1]]
    tw2 <- tempFS_objs[[2]]
    tw3 <- tempFS_objs[[3]]

    ## Extract data
    data_X1 <- tw1$data
    data_X2 <- tw2$data
    data_X3 <- tw3$data

    ## Extract the variables
    X1 <- data_X1[[exposure_var[1]]]
    X2 <- data_X2[[exposure_var[2]]]
    X3 <- data_X3[[exposure_var[3]]]

    ## Extract the slope
    a <- tw1$model$coefficients[[2]]
    b <- tw2$model$coefficients[[2]]
    c <- tw3$model$coefficients[[2]]



    merged_data <- dplyr::left_join(data_X1, data_X2)
    merged_data <- dplyr::left_join(merged_data, data_X3)

    Y <- lm(formula, data = merged_data)

    return(Y)

  } else {
    stop("Error : the function cannot support such a number of tempFS object. They must be 2 or 3")
  }




}











